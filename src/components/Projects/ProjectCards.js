import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {CgWebsite} from "react-icons/cg";
import {SiGitlab} from "react-icons/si";

const ProjectCards = (props) => {
    return (
        <Card className="project-card-view">
            <div className="card-content">
                <Card.Img variant="top" src={props.imgPath} alt="card-img" className="project-image"/>
                <Card.Body>
                    <Card.Title className="project-title">{props.title}</Card.Title>
                    <Card.Text style={{textAlign: "justify"}} className="project-description">
                        {props.description}
                    </Card.Text>
                </Card.Body>
            </div>
            <div className="card-buttons">
                <Button variant="primary" href={props.glLink} target="_blank">
                    <SiGitlab/> &nbsp;
                    {props.isBlog ? "Blog" : "Gitlab"}
                </Button>
                {!props.isBlog && props.demoLink && (
                    <Button
                        variant="primary"
                        href={props.demoLink}
                        target="_blank"
                        style={{marginLeft: "10px"}}
                    >
                        <CgWebsite/> &nbsp;
                        {"Demo"}
                    </Button>
                )}
            </div>
        </Card>
    );
}
export default ProjectCards;
