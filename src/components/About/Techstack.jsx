import React from "react";
import {Col, Row} from "react-bootstrap";
import {
    DiNodejs,
} from "react-icons/di";
import {
    SiGit,
    SiJavascript,
    SiLaravel,
    SiMongodb,
    SiMysql,
    SiPhp,
    SiPostgresql,
    SiPython,
    SiReact,
} from "react-icons/si";

const Techstack = () => {
    return (
        <Row style={{justifyContent: "center", paddingBottom: "50px"}}>
            <Col xs={4} md={2} className="tech-icons">
                <SiJavascript />
                <p className='techstack'>
                    Javascript <br />
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiReact />
                <p className='techstack'>
                    React <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <DiNodejs />
                <p className='techstack'>
                    NodeJs <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiPhp />
                <p className='techstack'>
                    PHP <br/>
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiLaravel />
                <p className='techstack'>
                    Laravel <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiMongodb />
                <p className='techstack'>
                    MongoDB <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiMysql />
                <p className='techstack'>
                    MySQL <br/>
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiPostgresql />
                <p className='techstack'>
                    PostgreSQL <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiGit />
                <p className='techstack'>
                    GIT <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiPython />
                <p className='techstack'>
                    Python <br/>
                    <span className="stars">★★☆☆☆</span>
                </p>
            </Col>
        </Row>
    );
}

export default Techstack;
