import React from "react";
import Card from "react-bootstrap/Card";
import { ImPointRight } from "react-icons/im";

const AboutCard = () => {
  return (
    <Card className="quote-card-view">
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p style={{ textAlign: "justify" }}>
            Hi Everyone, I am <span className="purple">Rivo Ramampiandra </span>
            from <span className="purple"> Antananarivo, Madagascar.</span><br />
            I am a developer graduated from the National Computer Science School (ENI) in Fianarantsoa. <br />
            Additionally, I am currently employed as a remote software developer at AGIS GROUP.
            <br />
            <br />
            Apart from coding, some other activities that I love to do!
          </p>
          <ul>
            <li className="about-activity">
              <ImPointRight/> Movies
            </li>
            <li className="about-activity">
              <ImPointRight/> Football
            </li>
            <li className="about-activity">
              <ImPointRight/> Playing Games
            </li>
          </ul>

          <p style={{color: "rgb(155 126 172)"}}>
            "If you don’t change, then what’s the point of anything happening to you?"{" "}
          </p>
          <footer className="blockquote-footer">Max Frisch</footer>
        </blockquote>
      </Card.Body>
    </Card>
  );
}

export default AboutCard;
