import React from "react";
import {Col, Row} from "react-bootstrap";
import {
    SiPostman,
    SiSlack,
    SiVercel,
    SiIntellijidea,
    SiGooglechat,
    SiUbuntu,
} from "react-icons/si";

const Toolstack = () => {
    return (
        <Row style={{justifyContent: "center", paddingBottom: "50px"}}>
            <Col xs={4} md={2} className="tech-icons">
                <SiUbuntu/>
                <p className='techstack'>
                    Ubuntu <br/>
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiIntellijidea/>
                <p className='techstack'>
                    IntelliJ <br/>
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiPostman />
                <p className='techstack'>
                    Postman <br/>
                    <span className="stars">★★★★☆</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiSlack />
                <p className='techstack'>
                    Slack <br/>
                    <span className="stars">★★★★★</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiGooglechat/>
                <p className='techstack'>
                    Google chat <br/>
                    <span className="stars">★★★★★</span>
                </p>
            </Col>
            <Col xs={4} md={2} className="tech-icons">
                <SiVercel />
                <p className='techstack'>
                    Vercel <br/>
                    <span className="stars">★★★☆☆</span>
                </p>
            </Col>
        </Row>
    );
}

export default Toolstack;
